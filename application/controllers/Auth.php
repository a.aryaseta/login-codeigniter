<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('m_auth');
	}

	public function index(){
		show_404();
	}

	public function login(){
		$email = $this->input->post('email');
		$pass = $this->input->post('password');
		$this->form_validation->set_rules(
			'email', 
			'Email', 
			'required|trim|valid_email'
		);
		$this->form_validation->set_rules(
			'password', 
			'Password', 
			'required|trim|min_length[8]',
			[
			'min_length' => 'Password too short!'
			]
		);		
		if($this->form_validation->run() == false){
			$data['title'] = 'Login';
			$this->load->view('inheritComponent/header_v', $data);
	        $this->load->view('auth/login_v.php');
			$this->load->view('inheritComponent/footer_v');
		}else{
			if($this->m_auth->login($email, $pass)) {
				redirect('dashboard');
			}else{
				$this->session->set_flashdata('message', '
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
				  Wrong Password!
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>');
				redirect('auth/login');
			}
		}
	}

	public function registration(){
		$this->form_validation->set_rules(
			'name', 
			'Name', 
			'required|trim'
		);
		$this->form_validation->set_rules(
			'email', 
			'Email', 
			'required|trim|valid_email|is_unique[users.email]',
			[
				'is_unique' => 'This email has already registered!'
			]
		);
		$this->form_validation->set_rules(
			'password1', 
			'Password', 
			'required|trim|min_length[8]|matches[password2]',
			[
			'matches' => 'Password does not match!',
			'min_length' => 'Password too short!'
			]
		);
		$this->form_validation->set_rules(
			'password2', 
			'Password', 
			'required|trim|matches[password1]'
		);
		if($this->form_validation->run() == false){
			$data['title'] = 'Register';
			$this->load->view('inheritComponent/header_v', $data);
			$this->load->view('auth/register_v');
			$this->load->view('inheritComponent/footer_v');			
		}else{
			$this->m_auth->regist();
		}
	}

	public function logout(){
		$this->m_auth->logout();
		$this->login();
	}
}

?>