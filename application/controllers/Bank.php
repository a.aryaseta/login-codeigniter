<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('m_bank');
		$this->load->model('m_auth');
	}

	public function index() {
		if(!$this->m_auth->current_user()){
			redirect('auth/login');
		}
		$data['title'] = 'Bank';
		$data['no'] = 1;
		$query = $this->m_bank->show();
		$current_user = $this->m_auth->current_user();
		$arrData = array(
			'banks' => $query,
			'current_user' => $current_user
		);
		$this->load->view('inheritComponent/header_v', $data);
        $this->load->view('content/bank_v.php', $arrData);
        $this->load->view('inheritComponent/btn-scroll_v');
		$this->load->view('inheritComponent/logout-modal_v.php');
		$this->load->view('inheritComponent/footer_v');
	}

	public function create() {
		$name = $this->input->post('nameOfBank');
		$accountNumber = $this->input->post('accountNumber');
		$current_user = $this->m_auth->current_user();
		$user_id = $current_user->id;
		$this->m_bank->save($name, $accountNumber, $user_id);
		redirect('bank');
	}
	
	public function edit($id) {
		$name = $this->input->post('nameOfBank');
		$accountNumber = $this->input->post('accountNumber');
		$current_user = $this->m_auth->current_user();
		$user_id = $current_user->id;
		$this->m_bank->update($name, $accountNumber, $user_id, $id);
		redirect('bank');
	}

	public function delete($id){
		$this->m_bank->delete($id);
		redirect('bank');
	}
}