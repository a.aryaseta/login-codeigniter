<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Income extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model(['m_auth','m_income','m_bank', 'm_institution']);
	}

	public function index() {
		if(!$this->m_auth->current_user()){
			redirect('auth/login');
		}
		$data['title'] = 'Income';
		$data['no'] = 1;
		$banks = $this->m_bank->show();
		$institutions = $this->m_institution->show();
		$current_user = $this->m_auth->current_user();
		$incomes = $this->m_income->show();
		$arrData = array(
			'current_user' => $current_user,
			'incomes' => $incomes,
			'institutions' => $institutions,
			'banks' => $banks
		);
		$this->load->view('inheritComponent/header_v', $data);
		$this->load->view('content/income_v.php', $arrData);
		$this->load->view('inheritComponent/logout-modal_v.php');
		$this->load->view('inheritComponent/footer_v');	
	}

	public function create() {
		$date = $this->input->post('date');
		$desc = $this->input->post('desc');
		$type = $this->input->post('type');
		$institution = $this->input->post('institution');
		$bank = $this->input->post('bank');
		$nominal = $this->input->post('nominal');
		$current_user = $this->m_auth->current_user();
		$user_id = $current_user->id;
		$this->m_income->save($date, $desc, $type, $institution, $bank, $nominal, $user_id);
		redirect('income');
	}

	public function delete($id){
		$this->m_income->delete($id);
		redirect('income');
	}
}