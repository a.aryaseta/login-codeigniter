<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Institution extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('m_auth');
		$this->load->model('m_institution');
	}

	public function index() {
		if(!$this->m_auth->current_user()){
			redirect('auth/login');
		}
		$data['title'] = 'Institution';
		$data['no'] = 1;
		$current_user = $this->m_auth->current_user();
		$query = $this->m_institution->show();
		$arrData = array(
			'current_user' => $current_user,
			'institutions' => $query);
		$this->load->view('inheritComponent/header_v', $data);
        $this->load->view('content/institution_v.php', $arrData);
        $this->load->view('inheritComponent/btn-scroll_v');
		$this->load->view('inheritComponent/logout-modal_v.php');
		$this->load->view('inheritComponent/footer_v');
	}

	public function create() {
		$name = $this->input->post('institution_name');
		$current_user = $this->m_auth->current_user();
		$user_id = $current_user->id;
		$this->m_institution->save($name, $user_id);
		redirect('institution');
	}
	
	public function edit($id) {
		$name = $this->input->post('institution_name');
		$current_user = $this->m_auth->current_user();
		$user_id = $current_user->id;
		$this->m_institution->update($name, $user_id, $id);
		redirect('institution');
	}

	public function delete($id){
		$this->m_institution->delete($id);
		redirect('institution');
	}
}