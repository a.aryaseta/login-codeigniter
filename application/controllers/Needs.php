<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Needs extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('m_auth');
		$this->load->model('m_needs');
	}

	public function index() {
		if(!$this->m_auth->current_user()){
			redirect('auth/login');
		}
		$data['title'] = 'Needs';
		$data['no'] = 1;
		$current_user = $this->m_auth->current_user();
		$query = $this->m_needs->show();
		$arrData = array(
			'current_user' => $current_user,
			'needs' => $query
		);
		$this->load->view('inheritComponent/header_v', $data);
        $this->load->view('content/needs_v.php', $arrData);
        $this->load->view('inheritComponent/btn-scroll_v');
		$this->load->view('inheritComponent/logout-modal_v.php');
		$this->load->view('inheritComponent/footer_v');
	}

	public function create() {
		$name = $this->input->post('needs_name');
		$current_user = $this->m_auth->current_user();
		$user_id = $current_user->id;
		$this->m_needs->save($name, $user_id);
		redirect('needs');
	}

	public function edit($id) {
		$name = $this->input->post('needs_name');
		$current_user = $this->m_auth->current_user();
		$user_id = $current_user->id;
		$this->m_needs->update($name, $user_id, $id);
		redirect('needs');
	}

	public function delete($id){
		$this->m_needs->delete($id);
		redirect('needs');
	}
}