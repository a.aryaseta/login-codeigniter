<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Outcome extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('m_auth');
	}

	public function index() {
		if(!$this->m_auth->current_user()){
			redirect('auth/login');
		}
		$data['title'] = 'Outcome';
		$current_user = $this->m_auth->current_user();
		$arrData = array('current_user' => $current_user);
		$this->load->view('inheritComponent/header_v', $data);
		$this->load->view('content/outcome_v.php', $arrData);
		$this->load->view('inheritComponent/logout-modal_v.php');
		$this->load->view('inheritComponent/footer_v');	
	}
}