<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('m_payment');
		$this->load->model('m_auth');
	}

	public function index() {
		if(!$this->m_auth->current_user()){
			redirect('auth/login');
		} 
		$data['title'] = 'Payment';
		$data['no'] = 1;
		$query = $this->m_payment->show();
		$current_user = $this->m_auth->current_user();
		$arrData = array(
			'payments' => $query,
			'current_user' => $current_user
		);
		$this->load->view('inheritComponent/header_v', $data);
        $this->load->view('content/payment_v.php', $arrData);
        $this->load->view('inheritComponent/btn-scroll_v');
		$this->load->view('inheritComponent/logout-modal_v.php');
		$this->load->view('inheritComponent/footer_v');
	}

	public function create() {
		$name = $this->input->post('payment_name');
		$current_user = $this->m_auth->current_user();
		$user_id = $current_user->id;
		$this->m_payment->save($name, $user_id);
		redirect('payment');
	}
	
	public function edit($id) {
		$payment_name = $this->input->post('payment_name');
		$current_user = $this->m_auth->current_user();
		$user_id = $current_user->id;
		$this->m_payment->update($payment_name, $user_id, $id);
		redirect('payment');
	}

	public function delete($id){
		$this->m_payment->delete($id);
		redirect('payment');
	}
}