<?php

class M_Auth extends CI_Model {

	const SESSION_KEY = 'user_id';

	public function regist(){
		$query = $this->db->get('users');
		$data = [
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'image' => 'default.jpg',
			'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
			'role_id' => 2,
			'is_active' => 1
		];
		$this->db->insert('users', $data);
		$this->session->set_flashdata('message', '
			<div class="alert alert-success alert-dismissible fade show" role="alert">
			  <strong>Congratulation!</strong> Your account has been created.
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>');
		redirect('auth/login');
		return $query->result();
	}
	
	public function login($email, $pass){
		$this->db->where('email', $email);
		$query = $this->db->get('users');
		$user = $query->row();
		if(!$user){
			return false;
		}
		if(!password_verify($pass, $user->password)){
			return false;
		}
		$this->session->set_userdata([self::SESSION_KEY => $user->id]);
		return $this->session->has_userdata(self::SESSION_KEY);
	}

	public function current_user(){
		if(!$this->session->has_userdata(self::SESSION_KEY)){
			return null;
		}
		$user_id = $this->session->userdata(self::SESSION_KEY);
		$query = $this->db->get_where('users', ['id' => $user_id]);
		return $query->row();
	}

	public function logout(){
		$this->session->unset_userdata(self::SESSION_KEY);
		return !$this->session->has_userdata(self::SESSION_KEY);
	}

}