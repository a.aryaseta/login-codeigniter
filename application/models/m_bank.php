<?php

class M_Bank extends CI_Model{
    public function __construct() {
        parent::__construct();
        $this->load->model('m_auth');
    }

    function show(){
        $query = $this->db->get_where('banks', array('user_id' => $this->m_auth->current_user()->id));
        return $query->result();
    }
 
    function save($name, $accountNumber, $user_id){
        $query = $this->db->get('banks');
        $data = [
            'name' => $name,
            'account_number' => $accountNumber,
            'user_id' => $user_id
        ];
        $this->db->insert('banks', $data);
        $this->session->set_flashdata('message', '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Congratulation!</strong> Your bank successfully created.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>');
        return $query->result();
        redirect('bank');
    }

    function update($name, $accountNumber, $user_id, $id) {
        $query = $this->db->get('banks');
        $data = [
            'name' => $name,
            'account_number' => $accountNumber,
            'user_id' => $user_id
        ];
        $this->db->where('id', $id);
        $this->db->update('banks', $data);
        $this->session->set_flashdata('message', '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Congratulation!</strong> Your bank successfully updated.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>');
        return $query->result();
        redirect('bank');
    }

    public function delete($id){
        $query = $this->db->get('banks');
        $this->db->where('id', $id);
        $this->db->delete('banks');
        $this->session->set_flashdata('message', '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Congratulation!</strong> Your bank successfully deleted.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>');
        return $query->result();
        redirect('bank');
    }

     
}