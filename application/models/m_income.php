<?php

class M_Income extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->model('m_auth');
	}

	public function show(){
    $this->db->select('incomes.id, incomes.date AS date, incomes.description AS description, incomes.type_income AS type, institutions.institution_name AS institution, banks.name AS bank, incomes.nominal AS nominal, incomes.user_id as user_id');
    $this->db->from('incomes');
    $this->db->join('institutions', 'incomes.institution_id = institutions.id');
    $this->db->join('banks', 'incomes.bank_id = banks.id');

    $query = $this->db->get();
    return $query->result();
	}

	public function save($date, $desc, $type, $institution, $bank, $nominal, $user_id){
        $query = $this->db->get('incomes');
        $data = [
            'date' => $date,
            'description' => $desc,
            'type_income' => $type,
            'institution_id' => $institution,
            'bank_id' => $bank,
            'nominal' => $nominal,
            'user_id' => $user_id
        ];
        $this->db->insert('incomes', $data);
        $this->session->set_flashdata('message', '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Congratulation!</strong> Your income successfully created.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>');
        return $query->result();
        redirect('income');
  }

  public function delete($id){
      $query = $this->db->get('incomes');
      $this->db->where('id', $id);
      $this->db->delete('incomes');
      $this->session->set_flashdata('message', '
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Congratulation!</strong> Your incomes successfully deleted.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>');
      return $query->result();
      redirect('income');
  }
}