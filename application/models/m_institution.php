<?php

class M_Institution extends CI_Model {
	public function __construct() {
        parent::__construct();
        $this->load->model('m_auth');
    }

	public function show() {
		$query = $this->db->get_where('institutions', array('user_id' => $this->m_auth->current_user()->id));
		return $query->result();
	}
 
    function save($name, $user_id) {
        $query = $this->db->get('institutions');
        $data = [
            'institution_name' => $name,
            'user_id' => $user_id
        ];
        $this->db->insert('institutions', $data);
        $this->session->set_flashdata('message', '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Congratulation!</strong> Your institution successfully created.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>');
        return $query->result();
        redirect('institution');
    }

    function update($name, $user_id, $id) {
        $query = $this->db->get('institutions');
        $data = [
            'institution_name' => $name,
            'user_id' => $user_id
        ];
        $this->db->where('id', $id);
        $this->db->update('institutions', $data);
        $this->session->set_flashdata('message', '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Congratulation!</strong> Your institution successfully updated.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>');
        return $query->result();
        redirect('institution');
    }

    public function delete($id) {
        $query = $this->db->get('institutions');
        $this->db->where('id', $id);
        $this->db->delete('institutions');
        $this->session->set_flashdata('message', '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Congratulation!</strong> Your institution successfully deleted.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>');
        return $query->result();
        redirect('institution');
    }
}