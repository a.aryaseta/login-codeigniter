<?php

class M_Needs extends CI_Model {
	public function __construct() {
        parent::__construct();
        $this->load->model('m_auth');
    }

	public function show() {
		$query = $this->db->get_where('needs', array('user_id' => $this->m_auth->current_user()->id));
		return $query->result();
	}

	public function save($name, $user_id){
		$query = $this->db->get('needs');
		$data = [
			'needs_name' => $name,
			'user_id' => $user_id
		];
		$this->db->insert('needs', $data);
        $this->session->set_flashdata('message', '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Congratulation!</strong> Your needs successfully created.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>');
        return $query->result();
        redirect('needs');
	}

	public function update($name, $user_id, $id){
		$query = $this->db->get('needs');
		$data = [
			'needs_name' => $name,
			'user_id' => $user_id
		];
		$this->db->where('id', $id);
		$this->db->update('needs', $data);
        $this->session->set_flashdata('message', '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Congratulation!</strong> Your needs successfully updated.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>');
        return $query->result();
        redirect('needs');
	}

	public function delete($id){
		$query = $this->db->get('needs');
		$this->db->where('id', $id);
		$this->db->delete('needs');
        $this->session->set_flashdata('message', '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Congratulation!</strong> Your needs successfully deleted.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>');
        return $query->result();
        redirect('needs');
	}
}