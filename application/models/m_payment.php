<?php

class m_payment extends CI_model {

	public function __construct() {
        parent::__construct();
        $this->load->model('m_auth');
    }

    function show(){
        $query = $this->db->get_where('payments', array('user_id' => $this->m_auth->current_user()->id));
        return $query->result();
    }
 
    function save($payment_name, $user_id){
        $query = $this->db->get('payments');
        $data = [
            'payment_name' => $payment_name,
            'user_id' => $user_id
        ];
        $this->db->insert('payments', $data);
        $this->session->set_flashdata('message', '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Congratulation!</strong> Your payment successfully created.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>');
        return $query->result();
        redirect('payment');
    }

    function update($payment_name, $user_id, $id) {
        $query = $this->db->get('payments');
        $data = [
            'payment_name' => $payment_name,
            'user_id' => $user_id
        ];
        $this->db->where('id', $id);
        $this->db->update('payments', $data);
        $this->session->set_flashdata('message', '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Congratulation!</strong> Your payment successfully updated.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>');
        return $query->result();
        redirect('payment');
    }

    public function delete($id){
        $query = $this->db->get('payments');
        $this->db->where('id', $id);
        $this->db->delete('payments');
        $this->session->set_flashdata('message', '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Congratulation!</strong> Your payment successfully deleted.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>');
        return $query->result();
        redirect('bank');
    }

}