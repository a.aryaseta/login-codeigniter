<div class="container">
    <div class="row justify-content-center">
        <div class="col-xl-5 col-lg-5 col-md-5">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Login Page!</h1>
                                </div>
                                <div id="flashMessage">
                                <?= $this->session->flashdata('message'); ?>
                                </div>
                                <form class="user" method="post" action="<?= base_url('auth/login'); ?>">
                                    <div class="form-group">
                                        <input type="email" class="form-control form-control"
                                            id="email" name="email" aria-describedby="emailHelp"
                                            placeholder="Email Address" value="<?= set_value('email'); ?>">
                                        <?= form_error('email','<small class="text-danger">','</small>')?>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control form-control"
                                            id="password" name="password" placeholder="Password">
                                        <?= form_error('password','<small class="text-danger">','</small>')?>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block">Login</button>
                                </form>
                                <hr>
                                <div class="text-center">
                                    <a class="small" href="forgot-password.html">Forgot Password?</a>
                                </div>
                                <div class="text-center">
                                    <a class="small" href="<?= base_url('auth/registration') ?>">Create an Account!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>