<div id="wrapper">
    <?php $this->load->view('inheritComponent/sidebar_v');?>
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            <?php $this->load->view('inheritComponent/navbar_v');?>
            <div class="container-fluid">
                <h1 class="h3 mb-2 text-gray-800">Income</h1>
                <p class="mb-4">This table is your income data. You can create and view, edit and delete your income data.</p>
                <div class="card shadow mb-4">
                    <div class="card-header py-3 px-3">
                        <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#createIncome"><i class="fas fa-plus mr-2"></i>Add
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Date</th>
                                        <th>Description</th>
                                        <th>Type</th>
                                        <th>From</th>
                                        <th>Where is it?</th>
                                        <th>Nominal</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($incomes as $row) { ?>
                                    <tr>
                                        <td class="text-center"><?= $no++ ?></td>
                                        <td><?= $row->date; ?></td>
                                        <td><?= $row->description; ?></td>
                                        <td><?= $row->type; ?></td>
                                        <td><?= $row->institution; ?></td>
                                        <td><?= $row->bank; ?></td>
                                        <td>Rp. <?= number_format($row->nominal); ?></td>
                                        <td class="text-center">
                                            <a class="btn btn-sm btn-warning" data-toggle="modal" data-target="#updateModal_<?=$row->id;?>"><i class="fas fa-pen"></i>
                                            </a>
                                            <a href="<?= base_url('income/delete/'.$row->id); ?>" onclick="return confirm('Are you sure to delete this data ?')" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="sticky-footer bg-white">
            <?php $this->load->view('inheritComponent/copyright_v');?>
        </footer>
    </div>
</div>

<div class="modal fade" id="createIncome" tabindex="-1" role="dialog" aria-labelledby="createIncome" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create Income</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url('income/create');?>" method="POST">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" for="date">Dates</span>
          </div>
          <input type="date" class="form-control" id="date" name="date">
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" for="desc">Description</span>
          </div>
          <input type="text" class="form-control" placeholder="Type here" id="desc" name="desc">
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <label class="input-group-text" for="type">Type</label>
          </div>
          <select class="custom-select" id="type" name="type">
            <option selected>Choose...</option>
            <option value="Salary">Salary</option>
            <option value="Fee">Fee</option>
            <option value="Project">Project</option>
            <option value="Loan">Loan</option>
          </select>
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <label class="input-group-text" for="institution">From</label>
          </div>
          <select class="custom-select" id="institution" name="institution">
            <option value="" disabled selected>Choose...</option>
            <?php foreach ($institutions as $row) { ?>
            <option value="<?=$row->id;?>"><?= $row->institution_name; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <label class="input-group-text" for="bank">Bank</label>
          </div>
          <select class="custom-select" id="bank" name="bank">
            <option value="" disabled selected>Choose...</option>
            <?php foreach ($banks as $row) { ?>
            <option value="<?=$row->id;?>"><?= $row->name; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" for="nominal">Nominal</span>
          </div>
          <input type="text" class="form-control" placeholder="Username" id="nominal" name="nominal">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </form>
    </div>
  </div>
</div>