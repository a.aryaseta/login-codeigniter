<div id="wrapper">
    <?php $this->load->view('inheritComponent/sidebar_v');?>
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            <?php $this->load->view('inheritComponent/navbar_v');?>
            <div class="container-fluid">
                <h1 class="h3 mb-2 text-gray-800">Payment</h1>
                <p class="mb-4">This table is your Payment data. You can create and view, edit and delete your Payment data.</p>
                <div class="card shadow mb-4">
                    <div class="card-header py-3 px-3">
                        <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#createModal"><i class="fas fa-plus mr-2"></i>Add
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <?= $this->session->flashdata('message'); ?>
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead class="text-center">
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($payments as $row) { ?>
                                    <tr>
                                        <td class="text-center"><?= $no++ ?></td>
                                        <td><?= $row->payment_name; ?></td>
                                        <td class="text-center">
                                            <a class="btn btn-sm btn-warning" data-toggle="modal" data-target="#updateModal_<?=$row->id;?>"><i class="fas fa-pen"></i>
                                            </a>
                                            <a href="<?= base_url('payment/delete/'.$row->id); ?>" onclick="return confirm('Are you sure to delete <?= $row->payment_name; ?> ?')" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="sticky-footer bg-white">
            <?php $this->load->view('inheritComponent/copyright_v');?>
        </footer>
    </div>
</div>

<div class="modal fade" id="createModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="createModal">Create Bank</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('payment/create');?>" method="POST">
                    <div class="form-group">
                        <label for="payment_name">Payment Name</label>
                        <input type="text" class="form-control" id="payment_name" name="payment_name" placeholder="Input here">
                    </div>              
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form> 
        </div>
    </div>
</div>

<?php foreach($payments as $row) { ?>
<div class="modal fade" id="updateModal_<?=$row->id;?>" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="updateModal">Update Payments</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('payment/edit/'.$row->id);?>" method="POST">
                    <div class="form-group">
                        <label for="payment_name">Name of Payment</label>
                        <input type="text" class="form-control" id="payment_name" name="payment_name" placeholder="Input here" value="<?= $row->payment_name; ?>">
                    </div>             
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<?php } ?> 