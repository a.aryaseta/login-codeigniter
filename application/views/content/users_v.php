<div id="wrapper">
    <?php $this->load->view('inheritComponent/sidebar_v');?>
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            <?php $this->load->view('inheritComponent/navbar_v');?>
            <div class="container-fluid">
                <h1 class="h3 mb-2 text-gray-800">Users</h1>
                <p class="mb-4">This table is your users data. You can create and view, edit and delete your users data.</p>
                <div class="card shadow mb-4">
                    <div class="card-header py-3 px-3">
                        <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#createOutcome"><i class="fas fa-plus mr-2"></i>Add
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Arya</td>
                                        <td>arya@gmail.com</td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-warning"><i class="fas fa-pen"></i>
                                            </button>
                                            <button type="button" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Arya</td>
                                        <td>arya@gmail.com</td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-warning"><i class="fas fa-pen"></i>
                                            </button>
                                            <button type="button" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Arya</td>
                                        <td>arya@gmail.com</td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-warning"><i class="fas fa-pen"></i>
                                            </button>
                                            <button type="button" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Arya</td>
                                        <td>arya@gmail.com</td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-warning"><i class="fas fa-pen"></i>
                                            </button>
                                            <button type="button" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="sticky-footer bg-white">
            <?php $this->load->view('inheritComponent/copyright_v');?>
        </footer>
    </div>
</div>

<div class="modal fade" id="createOutcome" tabindex="-1" role="dialog" aria-labelledby="createIncome" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create Income</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Dates</span>
          </div>
          <input type="date" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Description</span>
          </div>
          <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <label class="input-group-text" for="inputGroupSelect01">Type</label>
          </div>
          <select class="custom-select" id="inputGroupSelect01">
            <option selected>Choose...</option>
            <option value="1">Salary</option>
            <option value="2">Fee</option>
            <option value="3">Project</option>
          </select>
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <label class="input-group-text" for="inputGroupSelect01">From</label>
          </div>
          <select class="custom-select" id="inputGroupSelect01">
            <option selected>Choose...</option>
            <option value="1">Cash</option>
            <option value="2">Gopay</option>
            <option value="3">Ovo</option>
          </select>
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Cashback</span>
          </div>
          <input type="number" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Nominal</span>
          </div>
          <input type="number" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>