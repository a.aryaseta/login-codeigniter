<?php $this->load->view('inheritComponent/header_v');?>

<div id="wrapper">
    <?php $this->load->view('inheritComponent/sidebar_v');?>
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            <?php $this->load->view('inheritComponent/navbar_v');?>
            <div class="container-fluid">
                <h1 class="h3 mb-4 text-gray-800">Hello World!</h1>

            </div>
        </div>
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; Your Website 2020</span>
                </div>
            </div>
        </footer>
    </div>
</div>

<?php $this->load->view('inheritComponent/btn-scroll_v');?>
<?php $this->load->view('inheritComponent/logout-modal_v.php');?>
<?php $this->load->view('inheritComponent/footer_v');?>